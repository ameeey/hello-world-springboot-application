FROM openjdk:8-jdk-alpine
RUN addgroup -S Amey && adduser -S Amey -G Amey
USER Amey:Amey
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]